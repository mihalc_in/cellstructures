package caparzo.cellstructures;

import static caparzo.cellstructures.Grid.getGrid;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author caparzo
 */
public class GridTest {

    @Test
    public void sequenceNumberTest() {
        Grid grid = getGrid();

        int numOne = grid.getSequenceNumber();
        int numTwo = grid.getSequenceNumber();

        Assert.assertTrue(numOne == numTwo - 1);
    }
}
