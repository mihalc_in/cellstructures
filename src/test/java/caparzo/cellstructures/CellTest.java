package caparzo.cellstructures;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author caparzo
 */
public class CellTest {

    @Test
    public void twoAdjacentCellsTest() {
        Cell cell1 = new Cell(1, 1, Color.BLUE, " ");
        Cell cell2 = new Cell(1, 2, Color.BLUE, " ");

        Assert.assertTrue(cell1.isAdjacent(cell2));
    }

    @Test
    public void twoNonAdjacentVertexCellsTest() {
        Cell cell1 = new Cell(1, 1, Color.BLUE, " ");
        Cell cell2 = new Cell(0, 0, Color.BLUE, " ");

        Assert.assertTrue(!cell1.isAdjacent(cell2));
    }

    @Test
    public void twoNonAdjacentCellsTest() {
        Cell cell1 = new Cell(1, 1, Color.BLUE, " ");
        Cell cell2 = new Cell(3, 3, Color.BLUE, " ");

        Assert.assertTrue(!cell1.isAdjacent(cell2));
    }

    @Test
    public void adjacentToOneOfCellsTest() {
        Cell cell1 = new Cell(1, 1, Color.BLUE, " ");
        Cell cell2 = new Cell(1, 2, Color.BLUE, " ");
        List<Cell> cells = new ArrayList<>(2);
        cells.add(cell1);
        cells.add(cell2);
        Cell ourCell = new Cell(2, 2, Color.BLUE, " ");

        Assert.assertTrue(ourCell.isAdjacentToAny(cells));
    }

    @Test
    public void adjacentToNoneOfCellsTest() {
        Cell cell1 = new Cell(1, 1, Color.BLUE, " ");
        Cell cell2 = new Cell(1, 2, Color.BLUE, " ");
        List<Cell> cells = new ArrayList<>(2);
        cells.add(cell1);
        cells.add(cell2);
        Cell ourCell = new Cell(0, 0, Color.BLUE, " ");

        Assert.assertTrue(!ourCell.isAdjacentToAny(cells));
    }
}
