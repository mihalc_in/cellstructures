package caparzo.cellstructures.structure;

import caparzo.cellstructures.Cell;
import caparzo.cellstructures.Color;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author caparzo
 */
public class StructureTest {

    @Test
    public void adjacentStructuresTest() {
        Cell cell11 = new Cell(1, 1, Color.GREEN, "");
        Cell cell12 = new Cell(1, 2, Color.GREEN, "");
        AbstractStructure struct1 = new GreenStructure(cell11, cell12);
        Cell cell21 = new Cell(2, 2, Color.GREEN, "");
        Cell cell22 = new Cell(2, 3, Color.GREEN, "");
        AbstractStructure struct2 = new GreenStructure(cell21, cell22);
        Cell cell31 = new Cell(1, 0, Color.GREEN, "");
        Cell cell32 = new Cell(1, -1, Color.GREEN, "");
        AbstractStructure struct3 = new GreenStructure(cell31, cell32);

        Assert.assertTrue(struct1.isAdjacent(struct2));
        Assert.assertTrue(struct2.isAdjacent(struct1));
        Assert.assertTrue(struct1.isAdjacent(struct3));
    }

    @Test
    public void twoNonAdjacentStructuresTouchInVertexTest() {
        Cell cell11 = new Cell(1, 1, Color.GREEN, "");
        Cell cell12 = new Cell(1, 2, Color.GREEN, "");
        AbstractStructure struct1 = new GreenStructure(cell11, cell12);

        Cell cell21 = new Cell(2, 3, Color.GREEN, "");
        Cell cell22 = new Cell(2, 4, Color.GREEN, "");
        AbstractStructure struct2 = new GreenStructure(cell21, cell22);

        Assert.assertTrue(!struct1.isAdjacent(struct2));
        Assert.assertTrue(!struct2.isAdjacent(struct1));
    }

    @Test
    public void twoNonAdjacentStructuresSameColumnTest() {
        Cell cell11 = new Cell(1, 1, Color.GREEN, "");
        Cell cell12 = new Cell(1, 2, Color.GREEN, "");
        AbstractStructure struct1 = new GreenStructure(cell11, cell12);

        Cell cell21 = new Cell(1, 4, Color.GREEN, "");
        Cell cell22 = new Cell(1, 5, Color.GREEN, "");
        AbstractStructure struct2 = new GreenStructure(cell21, cell22);

        Assert.assertTrue(!struct1.isAdjacent(struct2));
        Assert.assertTrue(!struct2.isAdjacent(struct1));
    }

    @Test
    public void cellAdjacentToStructureTest() {
        Cell cell11 = new Cell(1, 1, Color.GREEN, "");
        Cell cell12 = new Cell(1, 2, Color.GREEN, "");
        AbstractStructure struct1 = new GreenStructure(cell11, cell12);
        Cell cellAdj = new Cell(0, 2, Color.BLUE, "");

        Assert.assertTrue(struct1.isAdjacent(cellAdj));
    }
}
