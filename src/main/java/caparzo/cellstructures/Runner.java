package caparzo.cellstructures;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import caparzo.cellstructures.io.StructureFileReader;
import caparzo.cellstructures.structure.StructureFactory;

/**
 * Class that reads structures from the file, validates them and writes info into the output file.
 *
 * @author caparzo
 */
public class Runner {

    private static final String DELIMITER = ",";

    /**
     * Reads structures from the file, validates them and writes info into the output file.
     *
     * @param args first argument is path to the input, second to the output
     */
    public static void main(String[] args) throws IOException {

        List<String> processedArgs = readArgs(args);

        StructureFileReader reader = new StructureFileReader(new StructureFactory());
        reader.readFile(new File(processedArgs.get(0)));

        FileWriter fw = new FileWriter(processedArgs.get(1));
        BufferedWriter bw = new BufferedWriter(fw);

        ValidationUtils.validateStructures(Grid.getGrid().getStructures(), bw, DELIMITER);
        ValidationUtils.validateGrid(Grid.getGrid(), bw, DELIMITER);

        bw.close();
        fw.close();
    }

    private static List<String> readArgs(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage:");
            System.out.println("  add 2 program arguments" +
                    " <inputFile> <resultFile>");
            throw new RuntimeException("");
        }
        List<String> processedArgs = new ArrayList<>();
        processedArgs.add(args[0]);
        processedArgs.add(args[1]);
        return Collections.unmodifiableList(processedArgs);
    }
}
