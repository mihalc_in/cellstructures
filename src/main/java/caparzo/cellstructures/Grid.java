package caparzo.cellstructures;

import static caparzo.cellstructures.Color.BLUE;
import static caparzo.cellstructures.ValidationUtils.appendValidationInfo;
import static caparzo.cellstructures.ValidationUtils.getValidationInfo;

import java.util.ArrayList;
import java.util.List;

import caparzo.cellstructures.structure.AbstractStructure;
import lombok.Getter;

/**
 * Grid which contains cells and structures and takes care of structures numbering.
 * It is a singleton.
 *
 * @author caparzo
 */
public class Grid implements Validable {
    @Getter
    private List<AbstractStructure> structures = new ArrayList<>();
    private int sequenceNumber = 0;

    private static Grid INSTANCE = new Grid();

    public static Grid getGrid() {
        return INSTANCE;
    }

    private Grid() {
    }

    /**
     * Returns a sequence number increased by one from the previously returned number.
     *
     * @return new sequence number
     */
    public int getSequenceNumber() {
        return sequenceNumber++;
    }

    /**
     * Adds a new structure to the grid.
     *
     * @param structure structure to add
     */
    public void addStructure(AbstractStructure structure) {
        structures.add(structure);
    }

    @Override
    public String validate() {
        StringBuilder sb = new StringBuilder("");

        // number of blue structures
        int blueCount = 0;
        for (AbstractStructure structure : structures) {
            if (BLUE == structure.getColor()) {
                blueCount++;
            }
        }
        if (blueCount > 2) {
            sb.append("more than two blue structures");
        }

        // average number of cells for structures of each color
        for (Color color : Color.values()) {
            if (averageCellsNumber(color) >= 5) {
                appendValidationInfo(sb, "average number of cells for the ")
                        .append(color.getName())
                        .append(" structures is 5 or more");
            }
        }
        return getValidationInfo(sb);
    }

    /**
     * Calculates average number of cells for the structures of provided color.
     *
     * @param color color of the structures
     * @return average number of cells
     */
    private double averageCellsNumber(Color color) {
        return structures.stream()
                .filter(s -> color == s.getColor())
                .mapToDouble(s -> s.getCells().size())
                .average()
                .orElse(0);
    }
}
