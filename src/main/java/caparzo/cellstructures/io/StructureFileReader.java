package caparzo.cellstructures.io;

import static caparzo.cellstructures.Grid.getGrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import caparzo.cellstructures.Cell;
import caparzo.cellstructures.Color;
import caparzo.cellstructures.structure.StructureFactory;
import lombok.RequiredArgsConstructor;

/**
 * Reads structures from a file and adding them into the grid.
 * Needs {@code StructureFactory} to create structures.
 *
 * @author caparzo
 */
@RequiredArgsConstructor
public class StructureFileReader {

    private final StructureFactory structureFactory;

    /**
     * Reads all cells from the provided file, constructs the structures and adds them to the grid.
     * Assumption: cells belonging to one grid are listed one after another, each cell adjacent to at least one of the previous cells.
     *
     * @param file file with the cells info
     */
    public void readFile(File file) throws FileNotFoundException {
        Scanner fileScanner = new Scanner(file);
        List<Cell> cells = new ArrayList<>();

        while (fileScanner.hasNextLine()) {
            Cell cell = readLine(fileScanner.nextLine());
            if (cells.isEmpty() ||
                    (cell.isAdjacentToAny(cells) &&
                            cell.getColor() == cells.get(cells.size() - 1).getColor()
                    )) {
                cells.add(cell);
            } else {
                getGrid().addStructure(structureFactory.createStructure(cells));
                cells.clear();
                cells.add(cell);
            }
        }
        if (!cells.isEmpty()) {
            getGrid().addStructure(structureFactory.createStructure(cells));
        }

        fileScanner.close();
    }

    /**
     * Reads a line from the file and creates a cell.
     *
     * @param line line with the cell info
     * @return a new cell
     */
    private Cell readLine(String line) {
        Scanner lineScanner = new Scanner(line);
        lineScanner.useDelimiter(",");
        int x = lineScanner.nextInt();
        int y = lineScanner.nextInt();
        Color color = Color.getColorByShortName(lineScanner.next());
        String comment = lineScanner.hasNext() ? lineScanner.next() : "";
        lineScanner.close();
        return new Cell(x, y, color, comment);
    }
}
