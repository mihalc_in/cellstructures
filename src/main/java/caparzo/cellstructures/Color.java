package caparzo.cellstructures;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Color enum.
 *
 * @author caparzo
 */
@Getter
@RequiredArgsConstructor
public enum Color {

    RED("Red", "R"),
    YELLOW("Yellow", "Y"),
    GREEN("Green", "G"),
    BLUE("Blue", "B"),
    BLANK("Blank", "");

    private final String name;
    private final String shortName;

    /**
     * Returns a color based on the short name provided in the file.
     *
     * @param shortName short name of the color
     * @return color
     */
    public static Color getColorByShortName(String shortName) {
        switch (shortName) {
            case "R":
                return RED;
            case "Y":
                return YELLOW;
            case "G":
                return GREEN;
            case "B":
                return BLUE;
            default:
                return BLANK;
        }
    }
}
