package caparzo.cellstructures.structure;

import static caparzo.cellstructures.Color.GREEN;
import static caparzo.cellstructures.ValidationUtils.appendValidationInfo;
import static caparzo.cellstructures.ValidationUtils.getValidationInfo;

import java.util.List;

import caparzo.cellstructures.Cell;
import caparzo.cellstructures.Color;

/**
 * Yellow structure.
 *
 * @author caparzo
 */
public class YellowStructure extends AbstractStructure {
    YellowStructure(List<Cell> cells) {
        super(Color.YELLOW, cells);
    }

    @Override
    public String validate() {
        StringBuilder sb = new StringBuilder("");

        if (hasAnyAdjacentStructureWithColor(GREEN)) {
            sb.append("there is adjacent green structure");
        }

        int x = getCells().get(0).getX();
        int y = getCells().get(0).getY();
        boolean linearX = true;
        boolean linearY = true;
        for (Cell cell : getCells().subList(1, getCells().size())) {
            if (cell.getX() != x) {
                linearX = false;
            }
            if (cell.getY() != y) {
                linearY = false;
            }
        }
        if (!linearX && !linearY) {
            appendValidationInfo(sb, "not linear");
        }
        return getValidationInfo(sb);
    }
}
