package caparzo.cellstructures.structure;

import static caparzo.cellstructures.Color.BLUE;

import java.util.List;

import caparzo.cellstructures.Cell;

/**
 * Blue structure.
 *
 * @author caparzo
 */
public class BlueStructure extends AbstractStructure {

    BlueStructure(List<Cell> cells) {
        super(BLUE, cells);
    }

    @Override
    public String validate() {
        return OK;
    }
}
