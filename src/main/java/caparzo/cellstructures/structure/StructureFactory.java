package caparzo.cellstructures.structure;

import java.util.List;

import caparzo.cellstructures.Cell;
import caparzo.cellstructures.Color;

/**
 * Factory for creating structures.
 *
 * @author caparzo
 */
public class StructureFactory {

    /**
     * Creates a new structure based on the color of the cells provided.
     * Color of the first cell is used.
     *
     * @param cells cells to form the structure
     * @return a new structure
     */
    public AbstractStructure createStructure(List<Cell> cells) {
        Color color = cells.get(0).getColor();
        switch (color) {
            case RED:
                return new RedStructure(cells);
            case BLUE:
                return new BlueStructure(cells);
            case YELLOW:
                return new YellowStructure(cells);
            case GREEN:
                return new GreenStructure(cells);
            default:
                break;
        }
        return null;
    }
}
