package caparzo.cellstructures.structure;

import static caparzo.cellstructures.Color.BLUE;
import static caparzo.cellstructures.Color.GREEN;
import static caparzo.cellstructures.ValidationUtils.getValidationInfo;

import java.util.Arrays;
import java.util.List;

import caparzo.cellstructures.Cell;

/**
 * Green structure.
 *
 * @author caparzo
 */
public class GreenStructure extends AbstractStructure {

    GreenStructure(List<Cell> cells) {
        super(GREEN, cells);
    }

    GreenStructure(Cell... cells) {
        this(Arrays.asList(cells));
    }

    @Override
    public String validate() {
        StringBuilder sb = new StringBuilder("");

        if (!hasAnyAdjacentStructureWithColor(BLUE)) {
            sb.append("no adjacent blue structure");
        }
        return getValidationInfo(sb);
    }
}
