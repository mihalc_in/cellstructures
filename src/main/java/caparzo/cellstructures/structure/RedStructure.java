package caparzo.cellstructures.structure;

import static caparzo.cellstructures.Color.RED;
import static caparzo.cellstructures.ValidationUtils.appendValidationInfo;
import static caparzo.cellstructures.ValidationUtils.getValidationInfo;

import java.util.List;

import caparzo.cellstructures.Cell;

/**
 * Red structure.
 *
 * @author caparzo
 */
public class RedStructure extends AbstractStructure {

    RedStructure(List<Cell> cells) {
        super(RED, cells);
    }

    @Override
    public String validate() {
        StringBuilder sb = new StringBuilder("");
        if (getAdjacentStructures().size() > 1) {
            sb.append("more than 1 adjacent structure");
        }
        if (getCells().size() > 5) {
            appendValidationInfo(sb, "more than 5 cells");
        }
        return getValidationInfo(sb);
    }
}
