package caparzo.cellstructures.structure;

import static caparzo.cellstructures.Grid.getGrid;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import caparzo.cellstructures.Cell;
import caparzo.cellstructures.Color;
import caparzo.cellstructures.Validable;
import lombok.Getter;
import lombok.ToString;

/**
 * Represents a structure in the grid.
 * Each structure has a unique sequence number.
 *
 * @author caparzo
 */
@ToString
public abstract class AbstractStructure implements Validable {
    @Getter
    private final Color color;
    @Getter
    private final List<Cell> cells;
    private final int sequenceNumber = getGrid().getSequenceNumber();

    AbstractStructure(Color color, List<Cell> cells) {
        this.color = color;
        this.cells = cells;
    }

    /**
     * Returns all adjacent structures.
     *
     * @return adjacent structures
     */
    List<AbstractStructure> getAdjacentStructures() {
        return getGrid()
                .getStructures()
                .stream()
                .filter(structure -> !equals(structure) && isAdjacent(structure))
                .collect(Collectors.toList());
    }

    boolean hasAnyAdjacentStructureWithColor(Color color) {
        return getGrid()
                .getStructures()
                .stream()
                .filter(structure -> color == structure.color)
                .anyMatch(structure -> !equals(structure) && isAdjacent(structure));
    }

    /**
     * Determines whether the other structure is adjacent to this.
     * Two structures are adjacent if there exist at least two cells (one from each structure) that are adjacent.
     *
     * @param other another structure
     * @return true if the two structures are adjacent, false otherwise
     */
    boolean isAdjacent(AbstractStructure other) {
        for (Cell cell : cells) {
            for (Cell otherCell : other.cells) {
                if (cell.isAdjacent(otherCell)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determines whether the structure is adjacent to the provided cell.
     *
     * @param otherCell cell to test
     * @return true if the cell is adjacent false otherwise
     */
    boolean isAdjacent(Cell otherCell) {
        for (Cell cell : cells) {
            if (cell.isAdjacent(otherCell)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractStructure)) return false;
        AbstractStructure that = (AbstractStructure) o;
        return sequenceNumber == that.sequenceNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sequenceNumber);
    }
}
