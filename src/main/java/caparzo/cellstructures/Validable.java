package caparzo.cellstructures;

/**
 * @author caparzo
 */
public interface Validable {

    String OK = "OK";

    /**
     * Validates rules of the object and returns the result as a String.
     * Structures and grids can be validated.
     *
     * @return validation result
     */
    String validate();
}
