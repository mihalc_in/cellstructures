package caparzo.cellstructures;

import static caparzo.cellstructures.Validable.OK;
import static java.lang.System.lineSeparator;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import caparzo.cellstructures.structure.AbstractStructure;

/**
 * Utility class containing methods related to validation.
 *
 * @author caparzo
 */
public class ValidationUtils {

    private ValidationUtils() {
    }

    /**
     * Appends validation info to the provided {@code StringBuilder}.
     * If the provided {@code StringBuilder} contains something, appends " and " first.
     * The method returns the provided {@code StringBuilder} so that it can be used with the following {@code StringBuilder.append} calls.
     *
     * @param sb       where to append
     * @param toAppend what to append
     * @return {@code StringBuilder} with appended info
     */
    public static StringBuilder appendValidationInfo(StringBuilder sb, String toAppend) {
        if (sb.length() > 0) {
            sb.append(" and ");
        }
        return sb.append(toAppend);
    }

    /**
     * Returns validation info
     *
     * @param sb validation info
     * @return validation info or {@link Validable#OK}
     */
    public static String getValidationInfo(StringBuilder sb) {
        return sb.length() > 0 ? sb.toString() : OK;
    }

    /**
     * Validates provided {@code Structures} and writes the results to the {@code Writer} with the provided delimiter.
     *
     * @param structures structures to validate
     * @param writer     where to write the results
     * @param delimiter  delimiter to use while writing the results
     */
    static void validateStructures(List<AbstractStructure> structures, Writer writer, String delimiter) throws IOException {
        for (AbstractStructure structure : structures) {
            String validationStr = structure.validate();
            for (Cell cell : structure.getCells()) {
                String result = cell.getX() +
                        delimiter +
                        cell.getY() +
                        delimiter +
                        cell.getColor().getShortName() +
                        delimiter +
                        validationStr +
                        delimiter +
                        cell.getComment();

                writer.write(result);
                writer.write(lineSeparator());
            }
        }
    }

    /**
     * Validates the provided {@code Grid} and writes the results to the {@code Writer} with the provided delimiter.
     *
     * @param grid      grid to validate
     * @param writer    where to write the results
     * @param delimiter delimiter to use while writing the results
     */
    static void validateGrid(Grid grid, Writer writer, String delimiter) throws IOException {
        String result = "Grid" + delimiter + grid.validate();
        writer.write(result);
    }
}
