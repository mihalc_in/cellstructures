package caparzo.cellstructures;

import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Class representing a cell.
 *
 * @author caparzo
 */
@Getter
@ToString
@RequiredArgsConstructor
public class Cell {

    private final int x;
    private final int y;
    private final Color color;
    private final String comment;

    /**
     * Determines whether two cells are adjacent.
     *
     * @param other another cell
     * @return true if the two cells are adjacent, false otherwise
     */
    public boolean isAdjacent(Cell other) {
        return (x == other.x &&
                (other.y == y + 1 || other.y == y - 1))

                ||

                (y == other.y &&
                        (other.x == x + 1 || other.x == x - 1));
    }

    /**
     * Determines whether the cell is adjacent to any one of provided cells.
     *
     * @param cells cells to test for adjacency
     * @return true if the cell is adjacent to at least one of the provided cells, false otherwise
     */
    public boolean isAdjacentToAny(List<Cell> cells) {
        for (Cell other : cells) {
            if (isAdjacent(other)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return x == cell.x &&
                y == cell.y &&
                color == cell.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, color);
    }
}
