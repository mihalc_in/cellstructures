# Color structures #
The objective of the exercise is to implement a program that analyzes color structures in a two-dimensional grid and validates, whether the structures follow a set of given rules.

## The Grid ##

Assume that you have a two-dimensional grid, whose cells are either blank or filled with one of the following four colors: **red, yellow, green, blue**. Two cells in the grid are **adjacent** if they share an edge, i.e. cells that share just a vertex are **not adjacent**. A group of all adjacent cells that are not blank and that are filled with the same color is called a **structure**. Two **structures are adjacent** if there exists a cell in the first structure that is adjacent to a cell in the second structure i.e. if the structures share at least one edge in the grid.

An example of the grid is below:

![grid.PNG](https://bitbucket.org/repo/Radyae/images/1331185325-grid.PNG)

This grid contains 5 structures: 1 green structure, 1 red structure, 1 yellow structure and 2 blue structures. Yellow structure #3 and blue structure #4 are adjacent. Red structure #2 and blue structure #5 are also adjacent. Blue structure #4 and blue structure #5 are not adjacent since they do not share any edge.

## The Rules ##
Structures in the grid should follow these rules:

1. Green structure always has an adjacent blue structure.
2. Red structure has no more than one another adjacent structure.
3. Yellow structure cannot have an adjacent green structure.
4. Red structure cannot consist from more than 5 cells.
5. Yellow structure is always linear, i.e. all cells that form the structure are on a single horizontal or vertical line.
6. There are no more than two blue structures in the grid.
7. For each color, average number of cells per structure is less than 5.

A structure that follows all applicable rules is **valid**. A structure that does not follow all the applicable rules is **invalid**.

## Solution Target ##
The program will read a situation in the grid from an input file; identify all structures in the grid; analyze whether each structure follows the rules described above and write this information into the output file.

## Input File ##
The input file contains description of the situation in the grid. The situation is described by providing X and Y coordinates and color of each non-blank cell in the grid in the form **X,Y,color**. Each cell is described on a separate line. Its coordinates are positive numbers that fit into integer data type. The color is represented by first capital letter i.e. R – red, Y – yellow, G – green, B – blue. The information about coordinates and color may be followed by a comment. The content of the comment is arbitrary and the code must not rely on any information given there.
The grid from the example above would be described e.g. by the following input file:

* 7,22,G, Green structure #1
* 11,22,R, Red structure #2 (cell 1/2)
* 12,22,R, Red structure #2 (cell 2/2)
* 7,24,Y, Yellow structure #3 (cell 1/3)
* 7,25,Y, Yellow structure #3 (cell 2/3)
* 8,25,Y, Yellow structure #3 (cell 3/3)
* 9,24,B, Blue structure #4 (cell 1/2)
* 9,25,B, Blue structure #4 (cell 2/2)
* 10,23,B, Blue structure #5 (cell 1/4)
* 11,23,B, Blue structure #5 (cell 2/4)
* 11,24,B, Blue structure #5 (cell 3/4)
* 11,25,B, Blue structure #5 (cell 4/4)

## Output File ##
The output file should contain result of the analysis. The file should list all non-blank cells in the same order as they were listed in the input file. For each cell the file should contain its X and Y coordinates and color in the form **X,Y,color** followed by status information, followed by the eventual comment from the input file. The status information should be **OK** if the cell belongs to a valid structure; otherwise this should be a suitable text which indicates the problem encountered.
An example of the output file for the example above is as follows:

* 7,22,G,No adjacent blue structure, Green structure #1
* 11,22,R,OK, Red structure #2 (cell 1/2)
* 12,22,R,OK, Red structure #2 (cell 2/2)
* 7,24,Y,Not linear, Yellow structure #3 (cell 1/3)
* 7,25,Y,Not linear, Yellow structure #3 (cell 2/3)
* 8,25,Y,Not linear, Yellow structure #3 (cell 3/3)
* 9,24,B,OK Blue structure #4 (cell 1/2)
* 9,25,B,OK Blue structure #4 (cell 2/2)
* 10,23,B,OK Blue structure #5 (cell 1/4)
* 11,23,B,OK Blue structure #5 (cell 2/4)
* 11,24,B,OK Blue structure #5 (cell 3/4)
* 11,25,B,OK Blue structure #5 (cell 4/4)